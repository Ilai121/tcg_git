using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    private bool dragging = false;
    private float distance;
    public Transform playArea;
    Vector3 startingPosition;

    void Start()
    {
        startingPosition = transform.position;
    }

    void OnMouseDown()
    {
        distance = Vector3.Distance(transform.position, Camera.main.transform.position);
        dragging = true;
    }

    void OnMouseUp()
    {
        dragging = false;

        if (Vector3.Distance(transform.position, playArea.position) < 5f)
        {
            PlayCard();
        }
        else transform.position = startingPosition;
    }

    void Update()
    {
        if (dragging)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 rayPoint = ray.GetPoint(distance);
            transform.position = new Vector3(rayPoint.x, rayPoint.y, 0);
        }
    }

    int i = 0;
    void PlayCard()
    {
        Debug.Log("Card played! " + ++i);
        // Add your code here for what happens when a card gets played.
        // For example, you could animate it moving fully into the play area, subtract its cost from the player's resources, etc.
    }
}
